﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    //Ограниченный контроллер
    [Route("identity")]
    [Authorize]
    public class IdentityController : ControllerBase
    {
        public IActionResult Get()
        {
            //Выдает Json c данными пользователя и дополнительными Claim-ами
            return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
        }
    }
}