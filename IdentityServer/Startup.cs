﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Test;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityServer
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            //Инциализация IS
            services.AddIdentityServer()
                //Сертификат разработчика
                .AddDeveloperSigningCredential()
                // Клиенты котором мы можем выдовать доступ
                .AddInMemoryClients(Config.GetClients())
                //Ресурс который описывает пользователя
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                //Ресурс который защишаем
                .AddInMemoryApiResources(Config.GetApiResources())
                //Тестовые пользователи которым даем доступ (необъязательно)
                .AddTestUsers(Config.GetTestUsers());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //Добавляем в конвейр
            app.UseIdentityServer();
        }
    }
}
