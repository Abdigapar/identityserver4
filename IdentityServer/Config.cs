﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace IdentityServer
{
    public class Config
    {
        //Client
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "webApiClient", 
                    ClientName = "Test Client",
                    AllowedGrantTypes = GrantTypes.Implicit,

                    ClientSecrets = new List<Secret>
                    {
                        new Secret("superKey".Sha256())
                    },
                    //область где клиенту разрешено запрашивать 
                    AllowedScopes = new List<string>{"customAPI.read"}

                }
            };
        }

        //Resource
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new IdentityResource
                {
                    Name = "role",
                    UserClaims = new List<string>{"role"},
                }
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource
                {
                    Name = "currentApi",
                    DisplayName = "Test API",
                    Description = "Test API Access",
                    UserClaims = new List<string>{"role"},
                    ApiSecrets = new List<Secret>{new Secret("superKey".Sha256())},
                    Scopes = new List<Scope>
                    {
                        new Scope("currentApi.read"),
                        new Scope("currentApi.write")
                    }
                }
            };
        }

        public static List<TestUser> GetTestUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "",
                    Username = "noyan",
                    Password = "123",
                    Claims = new List<Claim>
                    {
                        new Claim(JwtClaimTypes.Email,"asd@gmail.com"),
                        new Claim(JwtClaimTypes.Role, "admin")
                    }
                }
            };
        }
    }
}